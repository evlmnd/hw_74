const express = require('express');
const fs = require('fs');

const router = express.Router();

router.get('/', (req, res) => {
    const path = "./messages";
    const filesList = [];
    const messages = [];

    fs.readdir(path, (err, files) => {
        files.forEach(file => {
            filesList.push(path + '/' + file);
        });

        filesList.slice(-5).map(file => {
            const message = JSON.parse(fs.readFileSync(file));
            messages.push(message);
        });

        res.send(messages);
    });
});


router.post('/create', (req, res) => {
    console.log(req.body);
    const date = new Date();
    const fileName = `./messages/${date}.txt`;
    fs.writeFile(fileName, JSON.stringify(req.body), err => {
        if (err) {
            console.log(err);
        } else {
            console.log('saved');
        }
    });
    res.send({...req.body, datetime: date});
});

module.exports = router;